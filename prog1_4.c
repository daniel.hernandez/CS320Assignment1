#include<stdio.h>
#include<math.h>

#define PI 3.14159265

int main()
{
	printf("Assignment #1-4, Daniel Hernandez, dhernandez92105@yahoo.com\n");

	float size;
	float length;

	scanf("%f %f", &size, &length);

	for(int i = 0.0; i < size; i++)
	{
		printf("%.2f ", i*length);
	}

	printf("\n");

	for(int i = 0.0; i < size; i++)
        {
		printf("%.2f ", cos(i*(length * PI / 180.0)));
	}


}
