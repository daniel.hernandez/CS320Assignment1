CS320 ASSIGNMENT 1

---------
Objective
---------
This project was intended to familiarize ourselves with the process of developing solutions
to problems, compiling them on a linux VM, documenting these programs, and sending them to
the git repository correctly.

--------
Programs
--------
"prog1_1.c"
This program simply outputs the name of the person that created it.

	-------------
	Compile using
	-------------
	gcc prog1_1.c
	./a.out

"prog1_1.c"
This program takes input from the user (their name), and outputs it in a simple statement.

	------------- 
        Compile using
        -------------
        gcc prog1_2.c
        ./a.out

"prog1_3.c"
This program displays the hyperbolic cosine of a number entered by the user.

	------------- 
        Compile using
        -------------
        gcc prog1_3.c -o prog1_3 -lm
        ./prog1_3

"prog1_4.c"
This program displays the X and Y components of a cosine wave, given the wave length and
size (in degrees) from the user.

	-------------
        Compile using
        -------------
        gcc prog1_4.c -o prog1_4 -lm
        ./prog1_4
